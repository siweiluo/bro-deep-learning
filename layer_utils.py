'''
Some utils for layers.
'''

import torch
from torch import Tensor


def generate_mask(inputs: Tensor) -> Tensor:
    assert len(inputs.shape) == 3
    batch_size, seq_len, _ = inputs.size()
    mask = torch.tril(torch.ones((seq_len, seq_len),
                                 dtype=torch.bool), diagonal=0)
    mask = mask.unsqueeze(0).expand(batch_size, -1, -1)
    return mask
