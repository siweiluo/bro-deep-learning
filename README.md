# Bro Deep Learning

Transformer based coding.

## DOING
- transformer related layers implementation and unit test
- transformer model

## TODO
- Reformating according to [google_python_style_guide](https://google.github.io/styleguide/pyguide.html)
- Positional encoding
- Training 
- Datasets
- Models like bert/gpt
- Fine tuning
