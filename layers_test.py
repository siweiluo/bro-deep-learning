import unittest

import torch

import layers

MODEL_DIM = 512
HEAD_NUM = 8
HEAD_DIM = MODEL_DIM // HEAD_NUM
BATCH_NUM = 16
INPUT_LEN = 10


class TestMultiHeadAttention(unittest.TestCase):
    '''
    Class for testing MultiHeadAttention layer.
    '''

    def test_init(self):
        attention_layer = layers.MultiHeadAttention(
            model_dim=MODEL_DIM, head_num=HEAD_NUM)
        assert HEAD_DIM == attention_layer.head_dim
        assert HEAD_NUM == attention_layer.head_num
        assert MODEL_DIM == attention_layer.model_dim

    def test_attention_dimension(self):
        attention_layer = layers.MultiHeadAttention(
            model_dim=MODEL_DIM, head_num=HEAD_NUM)
        input_shape = [BATCH_NUM, INPUT_LEN, HEAD_NUM, HEAD_DIM]

        q = torch.rand([BATCH_NUM, INPUT_LEN, HEAD_NUM, HEAD_DIM])
        k = torch.rand([BATCH_NUM, INPUT_LEN, HEAD_NUM, HEAD_DIM])
        v = torch.rand([BATCH_NUM, INPUT_LEN, HEAD_NUM, HEAD_DIM])

        output = attention_layer.attention(q, k, v)
        assert input_shape == list(output.shape)

    def test_attention_mask(self):
        pass


if __name__ == '__main__':
    unittest.main()
