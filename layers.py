'''
Transformer basic layers.
'''

from torch import Tensor
import torch.nn as nn
import torch
import math


class MultiHeadAttention(nn.Module):

    ''' Multi-head attention in transformer block.'''

    def __init__(self, model_dim: int, head_num: int) -> None:
        super(MultiHeadAttention, self).__init__()
        self.model_dim = model_dim
        self.head_num = head_num
        self.head_dim = model_dim // head_num
        assert self.model_dim == self.head_num * self.head_dim
        self.scale_factor = 1 / math.sqrt(self.head_dim)

        self.w_q = nn.Linear(self.model_dim, self.model_dim)
        self.w_k = nn.Linear(self.model_dim, self.model_dim)
        self.w_v = nn.Linear(self.model_dim, self.model_dim)

    # input(k, q, v): batch, len, head_num, head_dim
    # input mask: batch, len, len
    # output: batch, len, head_num, head_dim
    def attention(self, q: Tensor, k: Tensor, v: Tensor, mask: Tensor = None) -> Tensor:
        # Trans to batch, head_num, len, head_dim
        q = q.transpose(1, 2)
        k = k.transpose(1, 2)
        v = v.transpose(1, 2)
        x = (torch.matmul(q, k.transpose(-1, -2)) *
             self.scale_factor)  # batch, head_num, len, len
        if mask:
            x = x.masked_fill(mask, 1e-9)
        # Each line of score: prob/attentin to the i_th feature(of v).
        score = torch.softmax(x, -1)
        res = torch.matmul(score, v)  # batch, head_num, len, head_dim
        res = res.transpose(1, 2)  # batch, len, head_num, head_dim
        return res

    # x: batch, len, model_dim
    # output: batch, len, model_dim
    def forward(self, query: Tensor, key: Tensor, value: Tensor,  mask: Tensor = None) -> Tensor:
        n_batch, seq_len = query.shape[0:2]
        q, k, v = (
            self.w_q.forward(query),
            self.w_k.forward(key),
            self.w_v.forward(value),
        )  # each shape: batch, seq_len, model_dim
        q = q.view(n_batch, seq_len, self.head_num, self.head_dim)
        k = k.view(n_batch, seq_len, self.head_num, self.head_dim)
        v = v.view(n_batch, seq_len, self.head_num, self.head_dim)
        output = self.attention(q, k, v, mask).view(
            n_batch, seq_len, self.model_dim)
        return output


class PositionwiseFeedForward(nn.Module):
    ''' fc of 2 layers after multi-head attention '''

    def __init__(self, model_dim: int, drop_ratio: float, dff_scale: int = 4) -> None:
        super(PositionwiseFeedForward, self).__init__()

        self.model_dim = model_dim
        self.dff_dim = model_dim * dff_scale
        self.drop_ratio = drop_ratio
        assert drop_ratio >= 0 and drop_ratio < 1
        assert int(self.dff_dim) == int(model_dim) * int(dff_scale)
        self.fc_layer1 = nn.Linear(self.model_dim, self.dff_dim)
        self.drop_layer = nn.Dropout(drop_ratio)
        self.fc_layer2 = nn.Linear(self.dff_dim, self.model_dim)

    def forward(self, x: Tensor) -> Tensor:
        return self.fc_layer2(self.drop_layer(self.fc_layer1(x)))


class EncoderLayer(nn.Module):
    ''' One encoder block: with a multi-head attention and a feedforwad layer'''

    def __init__(self, model_dim: int, head_num: int, drop_ratio: float = 0) -> None:
        super(EncoderLayer, self).__init__()

        self.head_num = head_num
        self.model_dim = model_dim
        self.drop_ratio = drop_ratio

        self.multi_head_attention = MultiHeadAttention(model_dim=self.model_dim,
                                                       head_num=self.head_num)
        self.layer_norm_1 = nn.LayerNorm(self.model_dim)
        self.feed_forward_layer = PositionwiseFeedForward(model_dim=self.model_dim,
                                                          drop_ratio=self.drop_ratio)
        self.layer_norm_2 = nn.LayerNorm(self.model_dim)

    def forward(self, x: Tensor) -> Tensor:
        x = self.multi_head_attention(x, x, x) + x
        x = self.layer_norm_1(x)
        x = self.feed_forward_layer(x) + x
        return self.layer_norm_2(x)


class DecoderLayer(nn.Module):
    ''' One decoder block: with a multi-head attention and a feedforwad layer'''

    def __init__(self, model_dim: int, head_num: int, drop_ratio: float = 0) -> None:
        super(DecoderLayer, self).__init__()

        self.head_num = head_num
        self.model_dim = model_dim
        self.drop_ratio = drop_ratio

        self.masked_multi_head_attention = MultiHeadAttention(model_dim=self.model_dim,
                                                              head_num=self.head_num)
        self.layer_norm_1 = nn.LayerNorm(self.model_dim)
        self.multi_head_attention = MultiHeadAttention(model_dim=self.model_dim,
                                                       head_num=self.head_num)
        self.layer_norm_2 = nn.LayerNorm(self.model_dim)
        self.feed_forward_layer = PositionwiseFeedForward(model_dim=self.model_dim,
                                                          drop_ratio=self.drop_ratio)
        self.layer_norm_3 = nn.LayerNorm(self.model_dim)

    def forward(self, x: Tensor, mem: Tensor, mask: Tensor) -> Tensor:
        # TODO: reduce this duplicated mask generation on each decode layer.
        x = self.masked_multi_head_attention(x, x, x, mask) + x
        x = self.layer_norm_1(x)
        x = self.multi_head_attention(x, mem, mem) + x
        x = self.layer_norm_2(x)
        x = self.feed_forward_layer(x) + x
        return self.layer_norm_3(x)
